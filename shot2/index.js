const express = require('express');
const app = express();
const shot = require('./lib/shot.js');

app.get('/', function(req, res){

  var opts = {
    url: req.query.url || 'https://google.com',
    delay: Number(req.query.delay) || 0,
    zoom: Number(req.query.zoom) || 0.9
  };

  shot(opts).then(function(data) {
    console.log('data', data);
    res.contentType('image/png');
    res.end(data, 'binary');
  });

});

app.get('/test', function(req, res){


  var site = req.query.url || 'https://google.com',
  var delay = Number(req.query.delay) || 0,
  var zoom = Number(req.query.zoom) || 0.9


  (async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.setViewport({
      width: 1200,
      height: 800,
      deviceScaleFactor: zoom
    });

    await page.goto(site);
    await page.waitFor(delay);
    // let img = await page.screenshot({path: 'shots/' + name + '.png'});
    let img = await page.screenshot();

    res.contentType('image/png');
    res.end(data, 'binary');

    browser.close();
  })();
});


app.listen(3000);
