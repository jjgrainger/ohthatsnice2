const puppeteer = require('puppeteer');
const url = require('url');

const site = process.argv[2] || "https://google.com";
const timeout = Number(process.argv[3]) || 0;
const zoom = Number(process.argv[4]) || 0.9;
const name = url.parse(site).hostname.split('.')[0];

console.log(name);

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.setViewport({
    width: 1200,
    height: 800,
    deviceScaleFactor: zoom
  });

  await page.goto(site);
  await page.waitFor(timeout);
  await page.screenshot({path: 'shots/' + name + '.png'});

  browser.close();
})();
