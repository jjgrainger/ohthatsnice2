const puppeteer = require('puppeteer');
const url = require('url');

module.exports = async function(opts) {

    const site = opts.url || "https://google.com";
    const timeout = Number(opts.delay) || 0;
    const zoom = Number(opts.zoom) || 0.9;

    const name = url.parse(site).hostname.split('.')[0];

    // (async () => {
      const browser = await puppeteer.launch();
      const page = await browser.newPage();

      await page.setViewport({
        width: 1200,
        height: 800,
        deviceScaleFactor: zoom
      });

      await page.goto(site);
      await page.waitFor(timeout);

      let img = await page.screenshot();
      // let img = await page.screenshot({path: 'shots/' + name + '.png'});

      await browser.close();

      console.log(img);
      return img;
    // })();
};
